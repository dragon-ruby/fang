# Fang

Helpers you can use in your DragonRuby Project!

## Goal / Mission / Requirements

Fang's goal is to collect "[pure](https://discord.com/channels/608064116111966245/1126320520133087324/1127352821151436861)" snippets that can be loaded/combined in exciting ways that don't require a lot of documentation, setup, or dependencies.

Helpers that can be called with input and provide an output. Not things that require your project to be setup explicitly setup to allow things to function.

A good example is Fang's goals is [rand.rb](rand.rb). Both `Fang.true_or_false` and `Fang.range_sample([1,2])` do not require other classes / variables or system setup.

## Usage

1. Create `fang` directory: `mkdir -p app/fang`
2. Download via `download_stb_rb_raw`
3. Add 'require ''` statements

### Issue with 5.0

Need to add a hash set, for startup [Discord Feedback](https://discord.com/channels/608064116111966245/895482347250655292/1127343537046945832)

```ruby
module GTK
  class Runtime
    module DownloadStbRb
      def download_stb_rb_print_usage
        puts <<~S
          * INFO: download_stb_rb usage.
          From the DragonRuby console enter:

          #+begin_src ruby
            $gtk.download_stb_rb GITHUB_URL_TO_FILE_ENDING_IN_RB
          #+end_src

          OR

          #+begin_src ruby
            $gtk.download_stb_rb USER_NAME, REPO_NAME, FILE_NAME_ENDING_IN_RB
          #+end_src

          If the code you are trying to download isn't a github repository, then
          consider using

          #+begin_src ruby
            $gtk.download_stb_rb_raw DOWNLOAD_URL_TO_TEXT_CONTENT, SAVE_PATH
          #+end_src
        S
      end

      def download_stb_rb_raw(download_url, save_path, metadata = {})
        entry = metadata.merge download_url: download_url, save_path: save_path

        entry_to_return = entry.copy
        @download_stb_rb_requests ||= {}
        @download_stb_rb_requests[download_url] = entry
        @download_stb_rb_requests[download_url].request = $gtk.http_get(download_url)
        entry_to_return
      end
    end
  end
end

```

### Rand

```ruby
$gtk.download_stb_rb_raw 'https://gitlab.com/dragon-ruby/fang/-/raw/main/rand.rb', 'app/fang/rand.rb'
require 'app/fang/rand.rb'
```

### Sprites

```ruby
$gtk.download_stb_rb_raw 'https://gitlab.com/dragon-ruby/fang/-/raw/main/rand.rb', 'app/fang/rand.rb'
require 'app/fang/rand.rb'

$gtk.download_stb_rb_raw 'https://gitlab.com/dragon-ruby/fang/-/raw/main/sprites.rb', 'app/fang/sprites.rb'
require 'app/fang/sprites.rb'
```

### FromNow

Helper to make scheduling callbacks a bit easier. Thanks mooff!

```ruby
$gtk.download_stb_rb_raw 'https://gitlab.com/dragon-ruby/fang/-/raw/main/from_now.rb', 'app/fang/from_now.rb'
require 'app/fang/from_now.rb'
```

```ruby
def tick(args)
  $args = args
  FromNow.tick
end

def some_helper
  20.frames.from_now { puts "hello world" }
end
```

## TODO

All the things

## Links

- [Guild Hall Toolmakers's Guild](https://discord.com/channels/608064116111966245/1126320520133087324)
