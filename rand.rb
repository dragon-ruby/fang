# =========================================
# General Randomization Helpers
# =========================================\
module Fang
  class << self
    # Random True or False
    def true_or_false
      rand(2).zero?
    end

    # Random Integer Negative
    # === [ Usage ] ===
    # Fang.rand_int_invert(5) # => -5
    # Fang.rand_int_invert(5) # => 5
    # Fang.rand_int_invert(5) # => -5
    def rand_int_invert(int = rand)
      true_or_false ? int : int * -1
    end

    # Random Integer between range
    # === [ Usage ] ===
    # Fang.randr(5,10) # => 6
    def randr(min, max)
      rand(max - min + 1) + min
    end

    # Sample value from a Range object
    # === [ Usage ] ===
    # Fang.range_sample(5..10, 3) # => [10,8,6]
    # Fang.range_sample(5..10) # => [1]
    def range_sample(range, count = 1)
      r = range.to_a.shuffle!
      count.times.map { r.shift }
    end

    # Helper to implement something similar to ruby's `sample(3)``
    # === [ Usage ] ===
    # Fang.array_sample([1,2,3], 2) # => [1,3]
    def array_sample(array, count = 1)
      array = array.clone

      result = []
      count.times do
        i = array.sample
        result.push i
        array.delete i
      end

      result
    end

    # ============================================================================
  end
  # ==============================================================================
end
