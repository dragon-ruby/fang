# =========================================
# General Helpers for Sprites
# =========================================
# == [ Requirements ] ==
# This uses the general Random Helpers (rand.rb)
# =========================================
module Fang
  class << self
    # Helpers to Randomize Sprite Stuff / Splat
    def sprite_randoms(list = nil)
      list ||= %i[
        angle_random
        flip_horizontally
        flip_vertically
      ]

      output = {}

      list.each do |kind|
        case kind
        when :angle_random
          output[:angle] = angle_random
        when :angle_90_random
          output[:angle] = angle_90_random
        when :flip_horizontally
          output[:flip_horizontally] = true_or_false
        when :flip_vertically
          output[:flip_vertically] = true_or_false
        end
      end

      output
    end

    # =========================================
    # Angle Adjusts
    # =========================================
    # Random Angle - Max 360, Max -360
    # === [ Usage ] ===
    # Fang.angle_random
    # { x: 0, y: 0, path: 'sprites/sprite.png', angle: Fang.angle_random }
    def angle_random(min = 0, max = 360)
      (min + rand(max)) * [1, -1].sample
    end

    # Randomly Return 90 rotation elements
    def angle_90_random
      rand_int_invert [270, 180, 90, 0].sample
    end

    # =========================================
    # Splattable XY Flip Random
    # =========================================
    # === [ Usage ] ===
    # { x: 0, y: 0, path: 'sprites/sprite.png', **Fang.flip_rand_xy }
    # { x: 0, y: 0, path: 'sprites/sprite.png', **Fang.flip_rand_x }
    def flip_rand_xy
      {
        flip_horizontally: true_or_false,
        flip_vertically: true_or_false
      }
    end

    def flip_rand_x
      {
        flip_horizontally: true_or_false
      }
    end
    # ============================================================================
  end
  # ==============================================================================
end
